(async () => {

    function getPosts(pageNumber = 1) {
        return fetch(`https://swapi.dev/api/planets/?page=${pageNumber}`)
            .then(res => res.json())
    }

    function getPost(id) {
        return fetch(`https://swapi.dev/api/planets/${id}`)
            .then(res => res.json())
    }

    function renderPosts(posts) {
        const cardsEl = document.querySelector('.cards')
        const postStringArr = posts.map((el) => {
            const elUrl = el.url
            let numEl = parseInt(elUrl.match(/\d+/))
            return `<div class="card" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">${el.name}</h5>
                <p class="card-text"><i>Диаметр:</i> ${el.diameter}</p>
                <p class="card-text"><i>Население:</i> ${el.population}</p>
                <p class="card-text"><i>Уровень гравитации:</i> ${el.gravity}</p>
                <p class="card-text"><i>Природные зоны:</i> ${el.terrain}</p>
                <p class="card-text"><i>Климатические зоны:</i> ${el.climate}</p>
                <a href="#" class="btn btn-primary js-open-modal" data-bs-toggle="modal" data-bs-target="#exampleModal" data-id="${numEl}">подробнее</a>
            </div>
        </div>`
        }).join('')

        cardsEl.innerHTML = postStringArr
    }

    async function renderPostInModal(post) {
        const films = post.films

        let arr = []
        let descriptionActor

        for (let i = 0; i < films.length; i++) {

            let title = ''
            let episode_id = ''
            let release_date = ''

            let result = await fetch(films[i]).then(res => res.json()).then(result => result)

            let characters = result.characters

            title += `Название: ${result.title}`
            episode_id += `Номер эпизода: ${result.episode_id}`
            release_date += `Дата выхода: ${result.release_date}`

            let actor = []
            let homeworld = []

            for (let i = 0; i < characters.length; i++) {

                let result1 = await fetch(characters[i]).then(res => res.json())

                actor.push(result1)

                let home = result1.homeworld
                let result2 = await fetch(home).then(res => res.json().then(result => result.name))

                homeworld.push({"homeworld": `${result2}`})
            }

            descriptionActor = actor.map((el, i) => {
                return `      
                        <ul>
                        <li><i>Имя:</i> ${el.name}</li>
                        <li><i>Пол:</i> ${el.gender}</li>
                        <li><i>День рождения:</i> ${el.birth_year}</li> 
                        <li><i>Наименование родного мира:</i> ${homeworld[i].homeworld}</li>              
                        </ul>
`
            }).join(' ')

            arr.push({
                "title": `${title}`,
                "episode_id": ` ${episode_id}`,
                "release_date": `${release_date}`,
            })
        }

        let descriptionFilms = arr.map((el) => {
            return `<ul>
               <li>${el.title}</li>
               <li>${el.episode_id}</li>
               <li>${el.release_date}</li>
            </ul>`
        }).join(' ')


        document.querySelector('.modal-title').innerHTML = post.name
        document.querySelector('.modal-body').innerHTML = `
                            <div class="card" style="width: 18rem;">
                                <div class="card-body">
                                    <h3 class="card-title">О планете:</h3>
                                    <p class="card-text"><i>Диаметр:</i> ${post.diameter}</p>
                                    <p class="card-text"><i>Население:</i> ${post.population}</p>
                                    <p class="card-text"><i>Уровень гравитации:</i> ${post.gravity}</p>
                                    <p class="card-text"><i>Природные зоны:</i> ${post.terrain}</p>
                                    <p class="card-text"><i>Климатические зоны:</i> ${post.climate}</p>
                                </div>
                            </div>
                                <h3>Список фильмов</h3>         
                                <ul>
                                     <li>${descriptionFilms}</li>     
                                </ul>                     
                            <button class="btn btn-primary" type="button" data-bs-toggle="collapse" 
                            data-bs-target="#collapseWidthExample" aria-expanded="false" aria-controls="collapseWidthExample">
                                Список персонажей
                            </button>
                        </p>
                        <div style="min-height: 120px;">
                            <div class="collapse collapse-horizontal" id="collapseWidthExample">
                                <div class="card card-body" style="width: 300px;">
                                    <li>${descriptionActor}</li>
                                </div>
                            </div>
                        </div>
        `

    }

    function renderPagination(pageSize, pageNumber, total) {
        const paginationEl = document.querySelector('.pagination')

        let lis = '<li class="page-item active"><a class="page-link" href="#" data-page="1">1</a></li>'

        for (let i = 1; i < Math.round(total / pageSize); i++) {
            lis += `<li class="page-item"><a class="page-link" href="#" data-page="${i + 1}">${i + 1}</a></li>`
        }

        const html = `<li class="page-item"><a class="page-link" href="#">Previous</a></li>
        ${lis}
        <li class="page-item"><a class="page-link" href="#">Next</a></li>
        `
        paginationEl.innerHTML = html
    }

    async function handlePaginator() {
        document.querySelector('.pagination').addEventListener('click', async (event) => {
            event.preventDefault()
            if (!event.target.classList.contains('page-link')) {
                return
            }

            document.querySelectorAll('.page-item').forEach((item) => {
                item.classList.remove('active')
            })
            event.target.parentNode.classList.add('active')

            const pageNumber = event.target.getAttribute('data-page')

            const {results} = await getPosts(pageNumber, pageSize)
            renderPosts(results)
        })
    }

    function handleModal() {

        async function addPost(e) {
            e.preventDefault()
            if (!e.target.classList.contains('js-open-modal')) {
                return
            }

            const postId = e.target.getAttribute('data-id')
            const post = await getPost(postId)
            if (post) {
                document.querySelector('.modal-body').innerHTML = `
                    <div class="progress" role="progressbar" aria-label="Animated striped example" aria-valuenow="75" 
                    aria-valuemin="0" aria-valuemax="100">
                      <div class="progress-bar progress-bar-striped progress-bar-animated" style="width: 75%"></div>
                    </div>
`
                document.querySelector('.modal-title').innerHTML = 'Loading...'
                document.querySelector('.btn-close').setAttribute('disabled', 'disabled')
                await renderPostInModal(post)
                document.querySelector('.btn-close').removeAttribute('disabled')
            }
        }

        document.querySelector('.cards').addEventListener('click', addPost)
    }

    handleModal()

    const pageNumber = 1
    const pageSize = 10
    const total = 60

    const {results} = await getPosts(1, pageSize)
    renderPosts(results)
    renderPagination(pageSize, pageNumber, total)

    await handlePaginator()
})()